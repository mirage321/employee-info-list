import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-employe-info',
  templateUrl: './employe-info.component.html',
  styleUrls: ['./employe-info.component.css']
})
export class EmployeInfoComponent implements OnInit {

  empRadio: string = 'all';

  @Output()
  childEvent: EventEmitter<string>  = new EventEmitter <string> ();

  @Input() allCount: number;
  @Input() adminCount: NumberConstructor;
  @Input() programmerCount: number;
  @Input() developerCount: number;

  constructor() { }

  ngOnInit() {
  }

  onEmpChoiceChange(){
    this.childEvent.emit(this.empRadio);
  }
}

  