export class Employee {
    id: number;
    name: string;
    role: string;
    salary: number;

    constructor(id?: number, name?: string, role?: string, salary?: number){
        this.id = id;
        this.name = name;
        this.role = role;
        this.salary = salary;
    }
}
