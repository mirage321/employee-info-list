import { Component, OnInit } from '@angular/core';
import { Employee } from '../employee';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-employe-list',
  templateUrl: './employe-list.component.html',
  styleUrls: ['./employe-list.component.css']
})
export class EmployeListComponent implements OnInit {

  employees = new Array<Employee>();
  emp:Employee= new Employee();
  url:string = 'http://localhost:9000/employees';
  selectedRole:string = 'all';

  constructor(private _http:HttpClient){
    this._http.get<Employee[]>(this.url).subscribe(x => (this.employees = x));
  }
  ngOnInit() {
  }

  onEmpChoiceChanged(role){
    this.selectedRole = role;
  }
  getAllCount(){
    return this.employees.length;
  }

  getAdminCount(){
    return this.employees.filter(x => (x.role =='Admin')).length;
  }

  getProgrammerCount(){
    return this.employees.filter(x => (x.role == 'Programmer')).length;
  }

  getDeveloperCount(){
    return this.employees.filter(x => (x.role == "Developer")).length;
  }

  createRow(){
    this._http.post(this.url, this.emp,{
      headers: new HttpHeaders({'Content-Type':'application/json'})
    }).subscribe(x=> console.log(x));
    this.emp = new Employee(); //Mirage removing the  Employee and create new instance to .emp
    alert('Employee Added Sucessfully');
  }

  retrieveRow(id: number){
    var updatedUrl = `${this.url}/${id}`;
    this._http.get<Employee>(updatedUrl).subscribe(x=> (this.emp =x));
  }

  updateRow(id: number){
    var updatedUrl = `${this.url}/${id}`;
    this._http.put(updatedUrl, this.emp, {
    headers: new HttpHeaders({'Content-Type':'application/json'})
    }).subscribe(x => console.log(x));
    this.emp = new Employee(); //Mirage removing the  Employee and create new instance to .emp
    alert('Employee updated Sucessfully');
  }
  deleteRow(id: number){
    var updatedUrl = `${this.url}/${id}`;
    this._http.delete(updatedUrl).subscribe(x => console.log(x));
    alert('Employee removed Sucessfully');
  }
}
