import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient} from '@angular/common/http'

import { AppComponent } from './app.component';
import { EmployeInfoComponent } from './employe-info/employe-info.component';
import { EmployeListComponent } from './employe-list/employe-list.component';

@NgModule({
  declarations: [
    AppComponent,
    EmployeInfoComponent,
    EmployeListComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpClientModule  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
